const moment = require('moment')

class Analyzer {
    analyzeCommission(data, { period, originDate }) {
        const {
            payload: { operations }
        } = data
        const brokerCommissionIterator = this.filterOperations(operations, {
            period,
            originDate,
            type: 'BrokerCommission'
        })
        const marginCommissionIterator = this.filterOperations(operations, {
            period,
            originDate,
            type: 'MarginCommission'
        })

        const broker = {
            total: {},
            operations: 0,
            query: brokerCommissionIterator.next().value
        }
        const margin = {
            total: {},
            operations: 0,
            distribution: {},
            query: marginCommissionIterator.next().value
        }

        for (const op of brokerCommissionIterator) {
            const amount = Math.abs(op.payment)
            if (broker.total[op.currency] == null) {
                broker.total[op.currency] = 0
            }
            broker.total[op.currency] += amount
            broker.operations += 1
        }

        for (const op of marginCommissionIterator) {
            const amount = Math.abs(op.payment)
            if (margin.total[op.currency] == null) {
                margin.total[op.currency] = 0
            }
            margin.total[op.currency] += amount
            margin.operations += 1

            if (margin.distribution[amount] == null) {
                margin.distribution[amount] = 0
            }
            margin.distribution[amount] += 1
        }

        return { broker, margin }
    }

    *filterOperations(operations, { period = 'month', originDate, type } = {}) {
        const filters = []
        let query = {}

        if (period) {
            const from = moment(originDate).startOf(period)
            const to = from
                .clone()
                .add({ [period]: 1 })
                .subtract({ seconds: 1 })
            query = { ...query, from, to, period }

            filters.push(op => {
                const date = moment(op.date)
                return date.isSameOrAfter(from) && date.isSameOrBefore(to)
            })
        }

        if (type) {
            filters.push(op => op.operationType === type)
        }

        yield query

        for (const op of operations) {
            const failedFilter = filters.find(filter => filter(op) === false)
            if (!failedFilter) {
                yield op
            }
        }
    }
}

module.exports = { Analyzer }
