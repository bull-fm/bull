const fs = require('fs')
const dotenv = require('dotenv')
const Commander = require('commander')
const moment = require('moment')
const table = require('table')

const pkg = require('../package.json')
const { Analyzer } = require('./analyzer')
const { formatMoney } = require('./utils')

dotenv.config()

const currencyMap = {
    USD: '$',
    EUR: '€',
    RUB: '₽'
}

const dateFormatMap = {
    day: 'ddd • DD MMM • YYYY',
    week: 'Wo • DD MMM • YYYY',
    month: 'MMM YYYY',
    year: 'YYYY'
}

class Cli {
    constructor() {
        this.program = new Commander.Command('bull')
        this.program.version(pkg.version)

        this.program
            .command('commission')
            .option('-f, --file <file>', 'Tinkoff OpenAPI operations JSON file')
            .option('-o, --origin <origin>', 'Origin date from which to calculate period')
            .option('-p, --period <period>', 'date period (day, week, month, year)')
            .description('Calculate broker commissions in a specified period')
            .action(this.onAnalyzeCommission)
    }

    async run() {
        await this.program.parseAsync(process.argv)
    }

    onAnalyzeCommission = async ({ file, origin, period }) => {
        let data
        if (file) {
            const json = await fs.promises.readFile(file)
            data = JSON.parse(json)
        } else {
            console.log('Please specify operations JSON file with -f option')
            return
        }

        const analyzer = new Analyzer()
        const { broker, margin } = analyzer.analyzeCommission(data, { period, originDate: origin })
        if (broker.operations == 0 && margin.operations === 0) {
            console.log('No operations found for specified period')
            return
        }

        const { currencies, columnIdxMap: currencyColumnIdxMap } = this.getCurrencyColumnList(
            broker.total,
            margin.total
        )

        const dateFormat = dateFormatMap[broker.query.period]
        const rows = [
            ['Commission Type', ...currencies, 'Number of operations'],
            [
                'Operation commission',
                ...this.prepareCurrencyAmountRow(broker.total, currencyColumnIdxMap),
                broker.operations
            ],
            [
                'Margin commission',
                ...this.prepareCurrencyAmountRow(margin.total, currencyColumnIdxMap),
                margin.operations
            ],
            [moment(broker.query.from).format(dateFormat), '', '', '']
        ]

        const currencyColumnsConfig = Object.fromEntries(
            Object.entries(currencyColumnIdxMap).map(([currency, idx]) => [
                idx + 1,
                { alignment: 'right' }
            ])
        )
        const config = {
            columns: {
                ...currencyColumnsConfig,
                [1 + (currencies.length - 1) + 1]: {
                    alignment: 'right'
                }
            },
            border: table.getBorderCharacters('norc'),
            drawHorizontalLine: (index, size) => {
                return index === 0 || index === 1 || index === size - 1 || index === size
            }
        }

        // console.log(JSON.stringify(rows, null, 4))
        console.log(table.table(rows, config))
    }

    getCurrencyColumnList(...moneyAmounts) {
        const encounteredCurrencies = moneyAmounts
            .map(Object.keys)
            .reduce((allCurrencies, amountCurrencies) => [...allCurrencies, amountCurrencies], [])
        const uniqueCurrencies = Object.keys(Object.fromEntries(encounteredCurrencies))
        const columnIdxMap = Object.fromEntries(
            uniqueCurrencies.map((currency, idx) => [currency, idx])
        )
        return { currencies: uniqueCurrencies, columnIdxMap }
    }

    prepareCurrencyAmountRow(moneyAmount, currencyColumnIdxMap) {
        const numCurrencies = Object.keys(currencyColumnIdxMap).length
        const row = [...Array(numCurrencies).fill('')]
        for (const [currency, amount] of Object.entries(moneyAmount)) {
            const idx = currencyColumnIdxMap[currency]
            row[idx] = formatMoney(amount, {
                currency: currencyMap[currency] || 'unknown',
                withSign: false
            })
        }
        return row
    }
}

module.exports = { Cli }
