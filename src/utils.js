function formatQuantity(
    rawQuantity,
    { prefix, precision, sign, withSign, trimZeroes = true } = {}
) {
    precision = precision || 2
    trimZeroes = trimZeroes || false

    if (rawQuantity == null || (typeof rawQuantity === 'string' && rawQuantity.length === 0)) {
        return ''
    }

    let quantity
    if (typeof rawQuantity === 'string') {
        if (rawQuantity == null || rawQuantity.length === 0) {
            return ''
        }
        quantity = Number.parseFloat(rawQuantity)
        if (Number.isNaN(quantity)) {
            return ''
        }
    } else {
        quantity = rawQuantity
    }

    sign = withSign ? (sign != null ? sign : quantity > 0 ? 1 : quantity < 0 ? -1 : 0) : undefined
    quantity = withSign != null ? Math.abs(quantity) : quantity
    let formatted = precision != null ? quantity.toFixed(precision) : String(quantity)

    const split = formatted.split('.')
    if (trimZeroes && split[1] === '0'.repeat(precision)) {
        formatted = split[0]
    } else if (!trimZeroes && (split[1] == null || split[1].length < precision)) {
        const numZeroes = precision - (split[1] != null ? split[1].length : 0)
        split[1] = split[1] + '0'.repeat(numZeroes)
        formatted = split.join('.')
    }

    formatted =
        (sign != null ? (sign > 0 ? '+' : sign < 0 ? '-' : '') + ' ' : '') +
        (prefix != null ? prefix + ' ' : '') +
        formatted.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
    return formatted
}

function formatMoney(
    rawQuantity,
    { precision, currency, sign, withSign = true, trimZeroes = false } = {}
) {
    return formatQuantity(rawQuantity, {
        precision,
        prefix: currency,
        sign,
        withSign,
        trimZeroes
    })
}

module.exports = { formatQuantity, formatMoney }
